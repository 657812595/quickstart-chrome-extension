var docElement = document.documentElement;

// 是否触发展示插件
chrome.extension.sendRequest(
    {type: "isshow", url: location.href},
    function (response) {
        if (response.isshow) {
            extension_load();
        }
    }
);

function extension_load() {
    chrome.extension.sendRequest(
        {type: "getOptions", url: location.href},
        function (response) {
            var os = response.optionstr;
            var on = document.getElementById('runner_options');
            if (!on) {
                var wr = document.createElement("span");
                wr.id = "runner_tool";
                wr.style.display = 'none';

                var conf = document.createElement("span");
                conf.id = "runner_config";
                conf.innerText = encodeURIComponent(response.conf);
                wr.appendChild(conf);

                on = document.createElement('span');
                on.id = 'runner_options';
                on.innerText = os;
                wr.appendChild(on);
                docElement.appendChild(wr);
                on.firstChild.addEventListener(
                    'DOMCharacterDataModified', function () {
                        var opts = this.parentElement.innerText;
                        chrome.extension.sendRequest({
                            type: 'setOptions', optionstr: opts
                        });
                    },
                    false
                );
                if (response.isShowUpdateTip) {
                    showUpdateTip();
                }

                var now = new Date().getTime(),
                    version = Math.floor(now / 1000000),
                    protocol = document.location.protocol,
                    loadUrl = protocol == 'https:' ? Runner.https_load_url
                        : Runner.http_load_url;

                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            var scriptSrc = xhr.responseText;
                            var scriptSrcObj = JSON.parse(scriptSrc);
                            loadJS(scriptSrcObj.src);
                        }
                    }
                };
                xhr.open('get', loadUrl + "?v=" + version);
                xhr.send();
            }
        }
    );

    //监听click事件，如果监听到是点击的是助手条上的设置按钮则打开设置页面
    document.addEventListener('click', function (event) {
        var target = event.target,
            maxAncestor = 3,
            className;
        while (maxAncestor && target) {
            if (target.className) {
                break;
            } else {
                target = target.parentElement;
                maxAncestor--;
            }
        }
        className = !!target ? (isString(target.className) ? target.className : '') : '';
        if (className.indexOf('-chrome-open-set') > -1) {
            chrome.extension.sendRequest(
                {type: "openSetPage", url: location.href},
                function (response) {
                }
            );
        }
    });
}

function showUpdateTip() {
}

function loadJS(url, onload) {
    var domscript = document.createElement('script');
    domscript.src = url;
    domscript.charset = 'utf-8';
    if (onload) {
        domscript.onloadDone = false;
        domscript.onload = onload;
        domscript.onreadystatechange = function () {
            if ("loaded" === domscript.readyState && domscript.onloadDone) {
                domscript.onloadDone = true;
                domscript.onload();
                domscript.removeNode(true);
            }
        };

    }
    document.getElementsByTagName('head')[0].appendChild(domscript);
}

function isString(str) {
    return toString.call(str) === '[object String]';
}